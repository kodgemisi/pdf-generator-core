# Creating OS Specific Library

Below the process is explained for `Linux` but it is almost the same for other OSes.

1. Create new folder named `deleteMe`
2. Download Node.js LTS 64-bit for Linux https://nodejs.org/en/download/
3. Extract downloaded Node.js into `deleteMe` folder
4. Rename the extracted folder's name from `node-vx.xx.x-linux-x64` to `node`
5. Install latest version of `puppeteer`via npm `./node/bin/npm install puppeteer` (Do this in `deleteMe` folder)
6. Zip the content of `deleteMe` folder and rename the zip file as `pdfGeneration_linux` and move the zip file into `src/main/resources` folder of the library.

```bash
$ cd deleteMe
$ ls
node  node_modules  package-lock.json
$ zip pdfGeneration_linux *
  adding: node/ (stored 0%)
  adding: node_modules/ (stored 0%)
  adding: package-lock.json (deflated 67%)
$ ls
node  node_modules  package-lock.json  pdfGeneration_linux.zip
```

Be sure that when `pdfGeneration_linux.zip` is extracted the files they are not in a wrapper directory. There should be `node` and `node_modules` directories and `package-lock.json` file.

![./contentOfTheZipFile.png](./contentOfTheZipFile.png)

# Design Decisions

## Why not using Maven Profiles for different OSes

Using Maven Profiles (in pdf-generator-core) would create a `pdf-generator-core` package which is binary-dependent to a specific OS. Then how a client 
require different version of `pdf-generator-core` artifact from repository?

