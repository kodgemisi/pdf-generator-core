/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sun.security.action.GetPropertyAction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.security.AccessController.doPrivileged;

/**
 * Created on April, 2018
 *
 * @author destan
 */
@Slf4j
public class PdfGenerator {

	private static final org.slf4j.Logger jsLog = org.slf4j.LoggerFactory.getLogger("com.kodgemisi.pdfgenerator.js_logger");

	static final String VERSION = PdfGenerator.class.getPackage().getImplementationVersion();

	private static final String DEFAULT_DIRECTORY_NAME = "pdfGeneration_" + VERSION;

	private static final String PDF_GENERATION_TOOL_PATH_ENV_VAR_NAME = "PDF_GENERATION_TOOL_PATH";

	private final Path thePath;

	private final ExecutorService executorService;

	private final ObjectMapper objectMapper = new ObjectMapper();

	private final AbstractToolsExtractor toolsExtractor;

	/**
	 * <p>This constructor sets {@code path} parameter as {@code null}.</p>
	 * <p>This constructor sets {@code deleteOnExit} parameter as {@code true}.</p>
	 * <p>This constructor uses {@code Executors.newCachedThreadPool()} by default.</p>
	 */
	public PdfGenerator() {
		this(null, Executors.newCachedThreadPool(), true);
	}

	/**
	 * <p>This constructor sets {@code path} parameter as {@code null}.</p>
	 * <p>This constructor uses {@code Executors.newCachedThreadPool()} by default.</p>
	 */
	public PdfGenerator(boolean deleteOnExit) {
		this(null, Executors.newCachedThreadPool(), deleteOnExit);
	}

	/**
	 * <p>This constructor sets {@code deleteOnExit} parameter as {@code true}.</p>
	 * <p>This constructor uses {@code Executors.newCachedThreadPool()} by default.</p>
	 *
	 * @param path cannot be null
	 */
	public PdfGenerator(@NonNull Path path) {
		this(path, Executors.newCachedThreadPool(), true);
	}

	/**
	 *
	 * @param path When given null, environment variable {@code PDF_GENERATION_TOOL_PATH}'s value is used. If there is no such environment variable
	 *                is defined then a new directory will be created with name {@link #DEFAULT_DIRECTORY_NAME} in system's temp directory
	 *                ({@code java.io.tmpdir}).
	 * @param executor cannot be null
	 * @param deleteOnExit
	 */
	public PdfGenerator(Path path, @NonNull ExecutorService executor, boolean deleteOnExit) {
		this.executorService = executor;

		String osName = System.getProperty("os.name") == null ? "null" : System.getProperty("os.name").toLowerCase();
		log.trace("OS name is {} in system properties.", osName);
		try {
			if (osName.startsWith("linux")) {
				osName = "linux";
				toolsExtractor = (AbstractToolsExtractor) Class.forName("com.kodgemisi.pdfgenerator.LinuxToolsExtractor").getDeclaredConstructor(Boolean.TYPE).newInstance(deleteOnExit);
			}
			else if (osName.startsWith("windows")) {
				osName = "windows";
				toolsExtractor = (AbstractToolsExtractor) Class.forName("com.kodgemisi.pdfgenerator.WindowsToolsExtractor").getDeclaredConstructor(Boolean.TYPE).newInstance(deleteOnExit);
			}
			else {
				throw new UnSupportedOperatingSystemException(osName);
			}
		}
		catch (IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
			throw new IllegalStateException(e);
		}
		catch (ClassNotFoundException e) {
			//TODO add a documentation link to this exception's message
			throw new MissingPlatformLibraryException("You should have used pdf-generator-" + osName + " library instead of pdf-generator-core.", e);
		}

		try {
			this.thePath = path == null ? getExtractionDirectory() : path;
			toolsExtractor.extractPdfGenerationTool(thePath);
		}
		catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	public CompletableFuture<Boolean> generatePdf(final URL url, final PdfGenerationOptions options) {
		final CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();

		executorService.submit(() -> {

			final List<String> commandArguments = toolsExtractor.getCommandArguments();

			commandArguments.add(url.toString());

			try {
				commandArguments.add(objectMapper.writeValueAsString(options));
			}
			catch (JsonProcessingException e) {
				log.error(e.getMessage(), e);
				completableFuture.completeExceptionally(e);
			}

			// ProcessBuilder is not thread safe so every call needs a new instance
			final ProcessBuilder processBuilder = new ProcessBuilder();
			final Process process;
			try {
				if (log.isDebugEnabled()) {
					log.debug("Calling process with arguments {}", commandArguments);
				}
				process = processBuilder.directory(thePath.toFile()).command(commandArguments).start();

				StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), process.getErrorStream());
				executorService.submit(streamGobbler);
				int exitCode = process.waitFor();

				completableFuture.complete(exitCode == 0);
			}
			catch (Exception e) {
				log.error(e.getMessage(), e);
				completableFuture.completeExceptionally(e);
			}
		});

		return completableFuture;
	}

	/**
	 * @return The configured path into which this instance is extracting PDF generation software.
	 */
	public Path getThePath() {
		return this.thePath;
	}

	private Path getExtractionDirectory() {
		final String destinationFromEnv = System.getenv(PDF_GENERATION_TOOL_PATH_ENV_VAR_NAME);

		if (destinationFromEnv != null && !destinationFromEnv.trim().isEmpty()) {
			log.info("{} is set to {} in environment. Using it...", PDF_GENERATION_TOOL_PATH_ENV_VAR_NAME, destinationFromEnv);
			final Path pathFromEnv = Paths.get(destinationFromEnv);
			if(!pathFromEnv.isAbsolute()) {
				throw new IllegalArgumentException(PDF_GENERATION_TOOL_PATH_ENV_VAR_NAME + " should define an absolute path!");
			}
			return pathFromEnv;
		}

		return getDefaultDirectoryName();
	}

	private Path getDefaultDirectoryName() {
		final String tmpdir = doPrivileged(new GetPropertyAction("java.io.tmpdir"));
		return Paths.get(tmpdir, DEFAULT_DIRECTORY_NAME);
	}

	//http://www.baeldung.com/run-shell-command-in-java
	private static class StreamGobbler implements Runnable {

		private InputStream inputStream;

		private InputStream errorStream;

		StreamGobbler(InputStream inputStream, InputStream errorStream) {
			this.inputStream = inputStream;
			this.errorStream = errorStream;
		}

		@Override
		public void run() {
			try (InputStreamReader is = new InputStreamReader(inputStream);
					InputStreamReader es = new InputStreamReader(errorStream);
					BufferedReader ibf = new BufferedReader(is);
					BufferedReader ebf = new BufferedReader(es);) {

				if (jsLog.isDebugEnabled()) {
					String input;
					while ((input = ibf.readLine()) != null) {
						jsLog.debug(input);
					}
				}

				String error;
				while ((error = ebf.readLine()) != null) {
					jsLog.error(error);
				}

			}
			catch (Exception e) {
				jsLog.error(e.getMessage(), e);
			}
		}
	}
}
