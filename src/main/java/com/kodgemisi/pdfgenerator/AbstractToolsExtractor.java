/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URL;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.PosixFilePermission;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created on April, 2018
 *
 * @author destan
 */
@Slf4j
abstract class AbstractToolsExtractor {

	//protected static final boolean isPosix = FileSystems.getDefault().supportedFileAttributeViews().contains("posix");

	protected final Set<PosixFilePermission> perms;

	private final String JS_FILE_NAME = "printToPdf.js";

	private final boolean deleteOnExit;

	AbstractToolsExtractor(boolean deleteOnExit) {
		HashSet<PosixFilePermission> permissions = new HashSet<>();
		permissions.add(PosixFilePermission.OWNER_EXECUTE);
		permissions.add(PosixFilePermission.OWNER_READ);

		this.perms = Collections.unmodifiableSet(permissions);
		this.deleteOnExit = deleteOnExit;
	}

	/**
	 * @param destinationPath extraction path
	 * @throws IOException
	 */
	protected void extractPdfGenerationTool(final Path destinationPath) throws IOException {
		final Instant start = Instant.now();

		if(destinationPath == null) {
			throw new IllegalArgumentException("destinationPath cannot be null");
		}

		createDestinationDirectoryIfNeeded(destinationPath);

		final File destinationDirectory = destinationPath.toFile();
		deleteOnExit(destinationDirectory);

		if(!destinationDirectory.exists()) {
			throw new FileNotFoundException(destinationDirectory.getAbsolutePath());
		}
		if(!destinationDirectory.isDirectory()) {
			throw new IllegalArgumentException(destinationDirectory.getAbsolutePath() + " is not a directory!");
		}
		if(destinationDirectory.list() != null && destinationDirectory.list().length > 0) {

			if(validPdfGenerationToolFilesFound(destinationPath)) {
				log.info("Valid PDF generation tool files found, skipping extraction...");
				return;
			}

			throw new IllegalStateException(destinationPath.toAbsolutePath() + " should be empty or should contain valid PDF generation tool files.");//TODO add documentation link
		}
		if(!destinationDirectory.canWrite()) {
			throw new AccessDeniedException(destinationDirectory.getAbsolutePath());
		}

		log.debug("Extracting PDF generation tool. Using destinationPath {}", destinationPath);

		byte[] buffer = new byte[1024];
		final URL url = getZipUrl();

		log.trace("Using zip file {}", url);

		try(final InputStream is = url.openStream();final ZipInputStream zis = new ZipInputStream(is)) {

			ZipEntry zipEntry = zis.getNextEntry();
			while(zipEntry != null) {
				final String fileName = zipEntry.getName();

				if(zipEntry.isDirectory()) {
					final Path newDir = Files.createDirectory(destinationPath.resolve(fileName));
					deleteOnExit(newDir.toFile());
				}
				else {
					final Path newFile = Files.createFile(destinationPath.resolve(fileName));
					deleteOnExit(newFile.toFile());

					try(FileOutputStream fos = new FileOutputStream(newFile.toFile());) {
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
					}

					if(newFile.toString().contains("/node/bin/") || newFile.toString().contains("/puppeteer/")) {
						final boolean executableResult = newFile.toFile().setExecutable(true);
						assert executableResult;
					}
				}
				zipEntry = zis.getNextEntry();
			}
		}

		if(log.isTraceEnabled()) {
			log.trace("Extracted in " + (Duration.between(start, Instant.now()).get(ChronoUnit.NANOS)/1_000_000) + "ms");
		}

		copyTheJavascriptFile(destinationPath.resolve(JS_FILE_NAME));

		createVersionFile(destinationPath);
	}

	private void createDestinationDirectoryIfNeeded(final Path destinationPath) {
		if(destinationPath.toFile().exists()) {
			log.trace("{} already exists, skipping directory creation.", destinationPath.toAbsolutePath());
			return;
		}

		try {
			Files.createDirectory(destinationPath);
		}
		catch (IOException e) {
			throw new IllegalStateException("Cannot create directory: " + destinationPath.toAbsolutePath(), e);
		}
	}

	private boolean validPdfGenerationToolFilesFound(final Path destinationDirectory) {
		final Path versionFilePath = getVersionFilePath(destinationDirectory);
		return versionFilePath.toFile().exists();//TODO check content
	}

	private void copyTheJavascriptFile(final Path targetPath) throws IOException {
		final URL url = getPrintToPdfFileUrl();
		deleteOnExit(targetPath.toFile());
		try(final InputStream is = url.openStream()) {
			Files.copy(is, targetPath);
		}
		log.trace("The javascript file is created {}", targetPath.toAbsolutePath());
	}

	private void createVersionFile(final Path destinationDirectory) throws IOException {
		final Path versionFilePath = getVersionFilePath(destinationDirectory);
		final List<String> content = Arrays.asList(PdfGenerator.VERSION, String.valueOf(System.currentTimeMillis()));
		Files.write(versionFilePath, content, StandardOpenOption.CREATE_NEW);
		deleteOnExit(versionFilePath.toFile());
		log.trace("Version file is created {}", versionFilePath.toAbsolutePath());
	}

	private Path getVersionFilePath(final Path destinationDirectory) {
		return destinationDirectory.resolve(PdfGenerator.VERSION + ".version");
	}

	private URL getPrintToPdfFileUrl() {
		return PdfGenerator.class.getResource("/" + JS_FILE_NAME);
	}

	private void deleteOnExit(File file) {
		if(this.deleteOnExit) {
			file.deleteOnExit();
		}
	}

	protected abstract URL getZipUrl();

	/**
	 *
	 * @return A list with initial capacity where there are 2 empty slots after platform dependent commands are appended to the list by implementing class.
	 */
	protected abstract List<String> getCommandArguments();

}
