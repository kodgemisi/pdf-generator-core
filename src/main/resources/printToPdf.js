//./node/bin/node printToPdf.js 'https://linuxcommando.blogspot.com.tr/2008/03/how-to-check-exit-status-code.html' '{}'
const puppeteer = require('puppeteer');
const url = require('url');
const path = require('path');
const os = require('os');

const HEADER_TEMPLATE = '<div style="width: 100%; font-size: 6pt; text-align: center;"><span class"title"></span></div>'; //'<span style="margin-top: -3mm;font-size: 5pt; text-align: center; width: 100%;"></span>';
const FOOTER_TEMPLATE = '<div style="margin-bottom: -2mm; margin-right: 5mm; height: 6pt; font-size: 6pt;text-align: right; width: 100%;"><span class="pageNumber"></span>/<span class="totalPages"></span></div>';

// `date`: formatted print date - `title`: document title -
// `url`: document location - `pageNumber`: current page number - `totalPages`: total pages in the document
// For example, `` would generate span containing the title.

const defaultOptions = {
    path: path.join(os.homedir(), randomString(5) + '.pdf'),
    format: 'A4',
    landscape: false,
    printBackground: true,
    margin: { // Paper margins, defaults to none.
        top: '0',
        bottom: '0',
        left: '0',
        right: '0'
    },
    displayHeaderFooter: false,
    headerTemplate: HEADER_TEMPLATE,
    footerTemplate: FOOTER_TEMPLATE,
    preferCSSPageSize: true
};

const options = parseOptions();
const urlAddress = parseUrl();

console.log('Generating PDF...\n', options, '\n', urlAddress);

// https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions
(async () => {
    const browser = await puppeteer.launch().catch(terminatingErrorHandler);
    const page = await browser.newPage().catch(terminatingErrorHandler);

    // These are not PDF printing options but we retrieve these options with the same option object for the sake of brevity.
    // Hence we need to delete these before passing options object to pdf generator to prevent errors.
    if(options.extraHttpHeaders){
        await page.setExtraHTTPHeaders(options.extraHttpHeaders);
        delete options.extraHttpHeaders;
    }

    if(options.timeout) {
        console.log('Setting timeout', options.timeout);
        // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagesetdefaultnavigationtimeouttimeout
        page.setDefaultNavigationTimeout(options.timeout);
        delete options.timeout;
    }

    if(options.emulateMedia){
        await page.emulateMedia(options.emulateMedia);
        delete options.emulateMedia;
    }

    await page.goto(urlAddress, {waitUntil: 'networkidle2'}).catch(terminatingErrorHandler);
    await page.pdf(options).catch(terminatingErrorHandler);

    console.log('PDF generated:', options.path);

    await browser.close().catch(terminatingErrorHandler);
})();

/*                                        FUNCTIONS                                        */
/* ========================================================================================*/

function parseUrl() {
    let userProvidedUrlAddress = process.argv[2];

    try {
        return url.parse(userProvidedUrlAddress).href; // in order to validate the url address
    }
    catch(e) {
        console.error('[ERROR] error while parsing userProvidedUrlAddress:\n', userProvidedUrlAddress);
        throw e;
    }
}

function parseOptions() {
    let optionsFromUser = process.argv[3];

    if(optionsFromUser) {
        try {
            optionsFromUser = JSON.parse(optionsFromUser);
        }
        catch(e) {
            console.error('[ERROR] user provided options:\n', optionsFromUser);
            console.error(e);
            optionsFromUser = {};
        }
    }

    // console.log('optionsFromUser:\n', optionsFromUser);
    const options = Object.assign({}, defaultOptions, optionsFromUser);

    // console.log('final options:\n', options);
    return options;
}

function terminatingErrorHandler(err) {
    console.error(err);
    process.exit(1);
}

// Thanks http://stackoverflow.com/a/10727155/878361
function randomString(length) {
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}
