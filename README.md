# PDF Generator - Java PDF Generation Library

This is a Java [Puppeteer](https://github.com/GoogleChrome/puppeteer) wrapper library. It comes with Nodejs and Chromium binaries for Linux and Windows 
and extracts them into operating system's preferred `temp` directory. This extraction path can be overridden via `PDF_GENERATION_TOOL_PATH` environment 
variable.

Currently only tested on Linux (Ubuntu 16.04 64bit).

# Usage

Users of this library shouldn't directly use this artifact. Instead they should choose one of the following artifacts according to their target OS family:
 
* [pdf-generator-linux](https://bitbucket.org/kodgemisi/pdf-generator-linux) 
* [pdf-generator-windows](https://bitbucket.org/kodgemisi/pdf-generator-windows)

Above artifacts already have dependency on `pdf-generator-linux` so you don't need to add `pdf-generator-linux` directly to your `pom.xml`.

# LICENSE

&copy; Copyright 2018 Kod Gemisi Ltd.

Mozilla Public License 2.0 (MPL-2.0)

https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)

MPL is a copyleft license that is easy to comply with. You must make the source code for any of your changes available under MPL, but you can combine 
the MPL software with proprietary code, as long as you keep the MPL code in separate files. Version 2.0 is, by default, compatible with LGPL and GPL 
version 2 or greater. You can distribute binaries under a proprietary license, as long as you make the source available under MPL.

[See Full License Here](https://www.mozilla.org/en-US/MPL/2.0/)